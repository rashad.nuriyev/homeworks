import React,{forwardRef,useImperativeHandle} from "react";
import ReactDOM from "react-dom";
import Proptypes from "prop-types";
import './Modal.scss';
const Modal = forwardRef(
    (props,ref) => {
        const [show,setShow] = React.useState(false);
        const open = () => { setShow(true) };
        const close = () => { setShow(false) };
        useImperativeHandle(ref,()=>{ return{    openModal: () => open()   }  });
        if (show){
            return ReactDOM.createPortal(
                <div className={"modal-wrapper"}>
                    <div onClick={close} className="modal-overlay"/>
                    <div className="modal-content">
                        <div className="modal-head">
                            <h5>{props.head}</h5>
                            <i onClick={close} className="fas fa-times"></i>
                        </div>
                        <div className="modal-body">
                            <p>{props.text}</p>
                            <div className="modal-button">
                                <button onClick={close} className="modal-button-btn">Ok</button><button onClick={close} className="modal-button-btn btn-blue">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                ,document.getElementById("modal"))
        }
        return null;

    }
);
Modal.propTypes={ head : Proptypes.string.isRequired }
Modal.propTypes={ text : Proptypes.string.isRequired }

export default Modal;