import React from 'react';
import './Button.scss';
const Button = (props)=>{
    const onClickHandler = () =>{
        props.clickHandler.current.openModal();
    }
    return (
        <button style={{backgroundColor:props.bgColor}} onClick={onClickHandler} className={"btn"}>{props.text}</button>
    )
}
export default Button;
