The Differences Between let and var
A var variable can be redeclared and updated.
A let variable be be updated but not redeclared.
An example of trying to redeclare a let variable:
// In editor:
<script>
let points = 50;
let points = 60;
</script>
// In the console I get an error:
Uncaught SyntaxError: Identifier 'points' has already been declared
However, we can update it:
let points = 50;
points = 60;
// In console:
points
// Returns:
60
------------------------------------
The Differences Between let and const
const variables cannot be updated. let variables are made to be updated.
// If I define the const variable:
const key = 'xyz123';
// Then try to redeclare it:
key = 'xyz1234'
// I get the following error:
Uncaught TypeError: Assignment to constant variable.
There is an interesting caveat to this, though. 
If I create a const variable that is an object, the attributes of that object can be updated