Array.prototype.forEach(callback([value, index, array]), thisArg)
forEach() calls a provided callback function once for each element in an array in ascending order. It is not invoked for index properties that have been deleted or are uninitialized.

