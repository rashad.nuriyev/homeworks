const tabsClick = document.querySelectorAll(".tabs-title");
const tabsContent = document.querySelectorAll(".tabs-content-item");

function closeAll(tabsClick, tabsContent) {
    for (let btns of tabsClick) {
        btns.classList.remove("active");
    }
    for (let content of tabsContent) {
        content.hidden = true;

    }
};

function showContents(e) {
    document.getElementById(e).hidden = false;
}

document.querySelector(".tabs").addEventListener('click', function (el) {
    if (!el.target.classList.contains("active")) {
        closeAll(tabsClick, tabsContent);
        el.target.classList.add("active");
        showContents(el.target.dataset.content);

    }
})
